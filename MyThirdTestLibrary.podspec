

Pod::Spec.new do |s|
  s.name             = 'MyThirdTestLibrary'
  s.version          = '0.1.0'
  s.summary          = 'A short description of MyThirdTestLibrary.'


  s.description      = <<-DESC
    这个是用于测试使用
                       DESC

  s.homepage         = 'https://git.oschina.net/6253175234/MyThirdTestLibrary.git'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'lyj' => '625317534@qq.com' }
  s.source           = { :git => 'https://git.oschina.net/6253175234/MyThirdTestLibrary.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '8.0'

  s.source_files = 'MyThirdTestLibrary/Classes/**/*'
  
  # s.resource_bundles = {
  #   'MyThirdTestLibrary' => ['MyThirdTestLibrary/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
