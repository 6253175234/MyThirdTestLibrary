//
//  main.m
//  MyThirdTestLibrary
//
//  Created by li3299838665 on 03/22/2017.
//  Copyright (c) 2017 li3299838665. All rights reserved.
//

@import UIKit;
#import "lyjAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([lyjAppDelegate class]));
    }
}
