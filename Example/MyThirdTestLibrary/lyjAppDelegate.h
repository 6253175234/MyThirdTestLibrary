//
//  lyjAppDelegate.h
//  MyThirdTestLibrary
//
//  Created by li3299838665 on 03/22/2017.
//  Copyright (c) 2017 li3299838665. All rights reserved.
//

@import UIKit;

@interface lyjAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
