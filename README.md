# MyThirdTestLibrary

[![CI Status](http://img.shields.io/travis/li3299838665/MyThirdTestLibrary.svg?style=flat)](https://travis-ci.org/li3299838665/MyThirdTestLibrary)
[![Version](https://img.shields.io/cocoapods/v/MyThirdTestLibrary.svg?style=flat)](http://cocoapods.org/pods/MyThirdTestLibrary)
[![License](https://img.shields.io/cocoapods/l/MyThirdTestLibrary.svg?style=flat)](http://cocoapods.org/pods/MyThirdTestLibrary)
[![Platform](https://img.shields.io/cocoapods/p/MyThirdTestLibrary.svg?style=flat)](http://cocoapods.org/pods/MyThirdTestLibrary)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

MyThirdTestLibrary is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "MyThirdTestLibrary"
```

## Author

li3299838665, 625317534@qq.com

## License

MyThirdTestLibrary is available under the MIT license. See the LICENSE file for more info.
